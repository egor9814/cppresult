//
// Created by egor9814 on 26 Feb 2021.
//

#ifndef __egor9814__result_type_hpp__
#define __egor9814__result_type_hpp__

#include <exception>
#include <variant>
#include <functional>

template <typename T, typename E = std::exception>
class Result {
	static_assert(std::is_base_of_v<std::exception, E>, "type E must inherit std::exception");
	static_assert(!std::is_same_v<T, E>, "types T and E must not be a same");

	using result_type = std::variant<T, E, void*>;

	result_type mValue;

public:
	Result(const T &value) : mValue(value) {}

	Result(const E &error) : mValue(error) {}

	Result(const Result &r) : mValue(r.mValue) {}

	Result(Result &&r) noexcept : mValue(std::move(r.mValue)) {}

	Result &operator=(const Result &r) {
		if (this != &r) {
			mValue = r.mValue;
		}
		return *this;
	}

	Result &operator=(Result &&r) noexcept {
		if (this != &r) {
			mValue = nullptr;
			mValue.swap(r.mValue);
		}
		return *this;
	}

	Result &operator=(const T &value) noexcept {
		mValue = value;
		return *this;
	}

	Result &operator=(const E &error) noexcept {
		mValue = error;
		return *this;
	}

	[[nodiscard]] bool isValid() const noexcept {
		return mValue.index() != 2;
	}

	[[nodiscard]] bool isValue() const noexcept {
		return mValue.index() == 0;
	}

	[[nodiscard]] bool isError() const noexcept {
		return mValue.index() == 1;
	}

	const T &value() const {
		return std::get<T>(mValue);
	}

	const E &error() const {
		return std::get<E>(mValue);
	}

	template<typename R>
	R let(std::function<R(const Result<T, E> &)> &&f) const {
		return f(*this);
	}

	Result &apply(std::function<void(Result<T, E> &)> &&f) {
		f(*this);
		return *this;
	}

	const Result &ifSuccess(std::function<void(const T &)> &&f) const {
		if (isValue()) {
			f(value());
		}
		return *this;
	}

	const Result &ifError(std::function<void(const E &)> &&f) const {
		if (isError()) {
			f(error());
		}
		return *this;
	}
};

#endif //__egor9814__result_type_hpp__

