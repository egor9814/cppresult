#include <iostream>
#include <result_type.hpp>

Result<double> calculate(char op, double a, double b) {
	switch (op) {
		case '+':
			return a + b;
		case '-':
			return a - b;
		case '*':
			return a * b;
		case '/':
			if (b == 0) {
				return std::runtime_error("division by zero");
			}
			return a / b;
		default:
			return std::runtime_error("unsupported operation");
	}
}

int main() {
	calculate('+', 1, 10).ifError([](const std::exception &err) {
		std::cout << "expected result (" << err.what() << ")" << std::endl;
	}).ifSuccess([](const int &result) {
		std::cout << "1 + 10 = " << result << std::endl;
	});
	calculate('/', 10, 2).ifError([](const std::exception &err) {
		std::cout << "expected result (" << err.what() << ")" << std::endl;
	});
	calculate('/', 10, 0).ifSuccess([](const double &) {
		std::cout << "expected error on '10 / 0'" << std::endl;
	});
	calculate('&', 10, 1).ifSuccess([](const double &) {
		std::cout << "expected error on '10 & 1'" << std::endl;
	});
	auto integer = calculate('/', 5, 2).let<int>([](const Result<double> &r) -> int {
		return static_cast<int>(r.value());
	});
	std::cout << "int(5 / 2) = " << integer << std::endl;
	return 0;
}
